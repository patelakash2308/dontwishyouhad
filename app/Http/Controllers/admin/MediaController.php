<?php

namespace App\Http\Controllers\admin;

use App\Media;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MediaController extends Controller
{
    // Media Dashboard
    public function index(){
        $media = Media::get();
        return view('admin.media',['media' => $media]);
    }

    public function addMedia(){
        return view('admin.addmedia');
    }

    public function storeMedia(Request $request){

        // dd($request);
        $this->validate($request,[
            'title'=>'required',
            'link'=>'required',
            'media_date'=>'required',
            'description'=>'required'
        ]);

        if($request->upload_type == 1){

            if($request->file('image') != ""){
                $image = $request->file('image');
                $image_video = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = 'images/media';
                $image->move($destinationPath, $image_video);
            } else {
                $image_video = "";
            }

        } else {
                $image_video = $request->youtube_link;
        }

        Media::create([
            'title'=>$request->title,
            'description'=>$request->description,
            'link'=>$request->link,
            'media_date'=>$request->media_date,
            'upload_type'=>$request->upload_type,
            'image_video'=>$image_video,
            'status'=>1,
        ]);

        return redirect(route('admin.media'))->with('message', 'Media detail has been added Successfully.');

    }

    public function editMedia($id){
        $media=Media::find($id);
        return view('admin.editmedia',['media'=>$media]);
    }




}
