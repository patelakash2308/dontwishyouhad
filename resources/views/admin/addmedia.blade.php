@extends('layouts.main')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="col-md-10">
                            <h3 class="box-title"> Add Media</h3>
                        </div>
                        <div class="col-md-2 ">
                            <a href="javascript:void(0)" onclick="window.history.back();" class="btn btn-block btn-primary btn-sm">Back</a>

                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- form start -->
                    <form action="{{route('admin.storeMedia')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body">

                            <div class="row">
                                <div class="col-xs-12 {{ $errors->has('title') ? 'has-error':'' }}">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text" class="form-control" name="title" placeholder="Enter Title" value="{{old('title')}}">
                                        @if($errors->has('title'))
                                        <span class="help-block">{{$errors->first('title')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 {{ $errors->has('description') ? 'has-error':'' }}">
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea type="text" class="form-control" name="description" placeholder="Description"> {{old('description')}}</textarea>
                                        @if($errors->has('description'))
                                            <span class="help-block">{{$errors->first('description')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 {{ $errors->has('link') ? 'has-error':'' }}">
                                    <div class="form-group">
                                        <label for="link">Link</label>
                                        <input type="text" class="form-control" name="link" placeholder="Enter Link" value="{{old('link')}}">
                                        @if($errors->has('link'))
                                        <span class="help-block">{{$errors->first('link')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-6 {{ $errors->has('media_date') ? 'has-error':'' }}">
                                    <div class="form-group">
                                        <label for="media_date">Date</label>
                                        <input type="text" class="form-control" name="media_date" id="media_date" placeholder="Enter Date" value="{{old('media_date')}}">
                                        @if($errors->has('media_date'))
                                            <span class="help-block">{{$errors->first('media_date')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 {{ $errors->has('upload_type') ? 'has-error':'' }}">
                                <label for="description">Upload Type</label>
                                <!-- radio -->
                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="upload_type" id="upload_type" class="upload_type" value="1" checked>
                                            Image
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="upload_type" id="upload_type" class="upload_type" value="2">
                                            Youtube Link
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-6 image-div {{ $errors->has('image') ? 'has-error':'' }}">
                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <input type="file" class="form-control" name="image" placeholder="" value="{{old('image')}}">
                                    @if($errors->has('image'))
                                    <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-xs-6 youtube-link-div {{ $errors->has('youtube_link') ? 'has-error':'' }}" style="display: none;">
                                <div class="form-group">
                                    <label for="youtube_link">Youtube Link</label>
                                    <input type="text" class="form-control" name="youtube_link" placeholder="Enter Youtube Link" value="{{old('youtube_link')}}">
                                    @if($errors->has('youtube_link'))
                                        <span class="help-block">{{$errors->first('youtube_link')}}</span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
            <!--/.col (left) -->

        </div>

    </section><!-- /.content -->

</div>
<!-- /.content-wrapper -->
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function () {

        $('.upload_type').change(function(){
            var upload_type = $(this).val();
            if(upload_type == 1){
                $('.image-div').show();
                $('.youtube-link-div').hide();
            } else {
                $('.image-div').hide();
                $('.youtube-link-div').show();
            }
        });

        $('#media_date').datepicker({
            format: 'yyyy-mm-dd'
        });

    });
</script>
@endsection
