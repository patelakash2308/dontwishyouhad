
<div>
    Hello!<br><br>

    Welcome to Don't wish you had. <br><br>

     Account Informations : <br><br>

     Email ID : {{ $email }} <br><br>
    Please click <a href="{{route('resetPassword',['email'=>encrypt($email)])}}">here</a> link to reset password.<br><br>

    If you did not request to change your password, please notify us immediately at support@safebeyond.com.<br><br>

    Regards,<br>
    Smart-Limo - Admin
</div>
