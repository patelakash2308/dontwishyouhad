@extends('layouts.frontmain')

@section('content')

    <section class="getstarted_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading">
                        <h3>Reset Password</h3>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row get_start_box">
                <div class="col-md-offset-2 col-md-8 formdiv">
                    <div class="form-group" id="password-field">
                        <input class="form-control tbox" id="password" name="password" placeholder="Password" type="password">
                        <div class="text-danger  password-error" style="display: none;"></div>
                    </div>
                    <div class="form-group" id="confirmpass-field">
                        <input class="form-control tbox" id="confirmpass" name="password_confirmation" placeholder="Confirm Password" type="password">
                        <div class="text-danger confirmpass-error" style="display: none;"></div>
                    </div>
                    <div class="buttondiv">
                        <button type="submit"  class="btn btn-default sbtn">Update Password</button>

                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </section>
@endsection